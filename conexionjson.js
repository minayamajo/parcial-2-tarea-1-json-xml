function datos() {
    const dat = new XMLHttpRequest();
    dat.open('GET', 'JSON.TAREA.json', true);
    dat.send();
    dat.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            let data = JSON.parse(this.responseText);
            let bod = document.querySelector('#bod');
            bod.innerHTML = '';
            for (let elementos of data ) {
                bod.innerHTML += `
                <tr>
                <td>${elementos.id}</td>
                <td>${elementos.cedula}</td>
                <td>${elementos.nombre}</td>
                <td>${elementos.apellido}</td>
                <td>${elementos.direccion}</td>
                <td>${elementos.telefono}</td>
                <td>${elementos.correo}</td>
                <td>${elementos.curso}</td>
                <td>${elementos.paralelo}</td>
                </tr>
                `
            }
        }
    }
}
function eliminar(){
    window.location.href = 'estudiantes_json.html'
}
document.querySelector('#boton').addEventListener('click', datos);
document.querySelector('#borrar').addEventListener('click', eliminar);
